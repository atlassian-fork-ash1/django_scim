from django.contrib.auth.hashers import SHA1PasswordHasher


class OldSHA1PasswordHasher(SHA1PasswordHasher):
    """Mimics old versions of Django that used salt values with length 5."""
    algorithm = 'shaI'

    def salt(self):
        return '01234'
